extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()
var has_double_jumped = false
var has_dash = true
var current_direction = null
onready var sprite = get_node("AnimatedSprite")

var animation = "stand"

func get_input():
	velocity.x = 0
	if is_on_floor():
		animation = "diri_kanan"
		has_dash = true
	
	if Input.is_action_pressed('right'):
		if is_on_floor():
			animation = "jalan_kanan"
		sprite.flip_h = false
		current_direction = 'right'
		velocity.x += speed
		
	if Input.is_action_pressed('left'):
		if is_on_floor():
			animation = "jalan_kiri"
		sprite.flip_h = true
		current_direction = 'left'
		velocity.x -= speed
		
	if Input.is_action_just_pressed('up'):
		animation = "lompat_kanan"
		if is_on_floor():
			has_double_jumped = false
			velocity.y = jump_speed
			animation = "lompat_kanan"
		elif(!has_double_jumped):
			velocity.y = jump_speed
			has_double_jumped = true
			
	if Input.is_action_just_pressed('space'):
		if has_dash:
			if current_direction == 'right':
				velocity.x += speed * 20
			elif current_direction == 'left':
				velocity.x -= speed * 20
		if(!is_on_floor() and current_direction != null):
			has_dash = false
		current_direction = null
		
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)

		

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)




# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
