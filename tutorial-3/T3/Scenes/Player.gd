extends KinematicBody2D

var double_jump_on_cooldown = false
export (int) var jump_speed = -600
export (int) var drop_speed = 1200

export (int) var GRAVITY = 1200
export (int) var MAX_SPEED = 400
export (int) var FRICTION = 20
export (int) var ACCELERATION = 20

var velocity = Vector2.ZERO

func _get_input():
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	
	if is_on_floor():
		if Input.is_action_just_pressed("ui_up"):
			velocity.y = jump_speed
		double_jump_on_cooldown = false
	elif Input.is_action_just_pressed("ui_up") and not double_jump_on_cooldown:
		velocity.y = jump_speed
		double_jump_on_cooldown = true
	
	if Input.is_action_just_pressed("ui_down"):
		velocity.y = drop_speed
	
	if input_vector != Vector2.ZERO:
		velocity.x = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION).x
	else:
		velocity.x = velocity.move_toward(Vector2.ZERO, FRICTION).x

func _physics_process(delta):
	_get_input()
	velocity.y += delta * GRAVITY
	velocity = move_and_slide(velocity, Vector2.UP)

